export const environments = [
    "promiseq-velimir",
    "promiseq-dev3",
    "promiseq-dev4",
    "promiseq-dev5",
    "promiseq-dev6",
    "promiseq-dev7",
    "promiseq-dev8",
    "promiseq-dev9",
    "promiseq-dev10",
    "promiseq-dev11",
    "promiseq-dev12",
    "aoye-dev",
    "vue-firebase-tutorial-240809",
    "promiseq-production2",
    "promiseq-production3",
  ];

  export const allowedReceivingHosts = [
    'notify.promiseq.com'
  ]

  export const allowedSendingHosts = [
    "receive.promiseq.com",
    'notify.promiseq.com'
  ]

  export const region = 'europe-west1'