import { SMTPServerSession, SMTPServerAuthenticationResponse } from "smtp-server";

export interface SmtpUser {
    projectId: string;
    defaultKey: string;
    keys: string[]
}