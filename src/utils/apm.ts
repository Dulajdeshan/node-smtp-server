import elasticAPM from 'elastic-apm-node'

export const apm: elasticAPM.Agent = elasticAPM.start({
  metricsInterval: '0s',
  cloudProvider: 'none',
  serviceName: 'promiseq_smtp_server',
  secretToken: 'lVa5vsXLT62dkFtzad',
  serverUrl:
    'https://65c63f5a27a84c0596cfe1c9702d7288.apm.europe-west1.gcp.cloud.es.io/',
  environment: process.env.ELASTIC_ENVIRONMENT || 'development'
})

