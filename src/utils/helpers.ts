import { EmailAddress } from "mailparser";

export const isEmpty = (text?: string) => {
    if(!text || !(text.length > 0)) {
        return true;
    }
    return false;
}

export const getEmailAddressToText = (emailAddress: EmailAddress) => {
    if(emailAddress.name) {
        return `${emailAddress.name} <${emailAddress.address}>`;
    }else {
        return emailAddress.address
    }
}