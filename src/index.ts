import {
    SMTPServer,
    SMTPServerAuthentication,
    SMTPServerSession,
    SMTPServerAuthenticationResponse,
    SMTPServerDataStream,
    SMTPServerAddress,
} from 'smtp-server';
import { ParsedMail, simpleParser } from 'mailparser';
import * as fs from 'fs';
import axios from 'axios';
import dotenv from 'dotenv'
import {
    environments,
    region,
    allowedReceivingHosts,
    allowedSendingHosts,
} from './utils/constants';
import { isEmpty } from './utils/helpers';
import ReceiptService from './service.ts/ReceiptService';
import { SmtpUser } from './utils/interface';
import { apm } from './utils/apm';

dotenv.config()

const PORT = process.env.PORT || 587;

const handleParsedData = async (parsedMail: ParsedMail, user: SmtpUser) => {
    try {
        const receiptService = new ReceiptService(parsedMail, user);
        receiptService.createRequestQueue();
        const processQueueSpan = apm.startSpan('Process Request Queue')
        await receiptService.processRequestQueue();
        processQueueSpan?.end()
        apm.endTransaction()
    } catch (error: any) {
        apm.captureError(new Error(error))
        apm.endTransaction()
        console.log('ERROR', error);
    }
};

const onAuth = async (
    auth: SMTPServerAuthentication,
    session: SMTPServerSession,
    callback: (
        error?: Error | null,
        response?: SMTPServerAuthenticationResponse
    ) => void
) => {
    apm.startTransaction('SMTP Server');
    const username = auth.username;
    const password = auth.password;
    const authCheckSpan = apm.startSpan('Authentication Check')
    if (isEmpty(username) ||
    isEmpty(password) ||
    !environments.includes(username!))
    {
        const capturedError = new Error('Inaccurate Username or Password');
        authCheckSpan?.end()
        apm.captureError(capturedError);
        apm.endTransaction()
        return callback(capturedError);
    } else {
        try {
            const response = await axios.post(
                `https://${region}-${username}.cloudfunctions.net/getUserBySmtpCredentials`,
                {
                    username: username,
                    password: password,
                }
            );
         
            authCheckSpan?.end()
            return callback(null, {
                user: JSON.stringify({
                    projectId: username,
                    defaultKey: response.data.defaultKey,
                    keys: response.data.keys,
                }),
            });
        } catch (err: any) {
            const capturedError = new Error(
                err.response?.data?.message || err.message
            );
            authCheckSpan?.end()
            apm.captureError(capturedError);
            apm.endTransaction()
            return callback(capturedError);
        }
    }
};

const onConnect = (
    session: SMTPServerSession,
    callback: (error?: Error | null) => void
) => {
    return callback();
};

const onData = (
    stream: SMTPServerDataStream,
    session: SMTPServerSession,
    callback: (error?: Error | null) => void
) => {
    const dataParseSpan = apm.startSpan('Parsing Data')
    simpleParser(stream, {}, (err, parsed) => {
        if (err) {
            console.log('Error:', err);
            const capturedError = new Error(err)
            dataParseSpan?.end()
            apm.captureError(capturedError)
            apm.endTransaction()
            return callback(capturedError)
        }
        if (session.user && typeof session.user === 'string') {
            const smtpUser: SmtpUser = JSON.parse(session.user);
            handleParsedData(parsed, smtpUser);
        }
    });
    stream.on('end', () => {
        dataParseSpan?.end()
        callback(null);
    });
};

const onMailFrom = (
    address: SMTPServerAddress,
    session: SMTPServerSession,
    callback: (error?: Error | null) => void
) => {
    const emailHost = address.address.split('@').pop();
    if (
        !session.user ||
        !emailHost ||
        !allowedReceivingHosts.includes(emailHost)
    ) {
        const capturedError = new Error('Unauthorized host')
        apm.captureError(capturedError)
        apm.endTransaction()
        return callback(capturedError);
    }
    return callback();
};

const onRcptTo = (
    address: SMTPServerAddress,
    session: SMTPServerSession,
    callback: (error?: Error | null) => void
) => {
    const emailHost = address.address.split('@').pop();
    if (!emailHost || !allowedSendingHosts.includes(emailHost)) {
        const capturedError = new Error('Unauthorized receipt address')
        apm.captureError(capturedError)
        apm.endTransaction()
        return callback(capturedError);
    }
    return callback();
};

const server = new SMTPServer({
    secure: false,
    key: fs.readFileSync(process.env.KEY_PATH || "./ssl/smtp.key"),
    cert: fs.readFileSync(process.env.CERT_PATH || "./ssl/smtp.crt"),
    onConnect: onConnect,
    onMailFrom: onMailFrom,
    onRcptTo: onRcptTo,
    onAuth: onAuth,
    onData: onData,
    banner: 'Hi! Welcome to promiseQ SMTP Server',
    logger: true
});

server.on('error', (err) => {
    console.log('On Error', err);
});

server.listen(PORT, () => {
    console.log('SMTP Server Started on PORT ' + PORT);
});
