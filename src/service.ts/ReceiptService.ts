import { ParsedMail } from 'mailparser';
import axios, { AxiosPromise } from 'axios';
import FormData from 'form-data';
import { region } from '../utils/constants';
import { SmtpUser } from '../utils/interface';

export default class ReceiptService {
    private parsedMail: ParsedMail;
    private smtpUser: SmtpUser;
    private attachments: {
        content: Buffer;
        fileName?: string;
        contentType: string;
    }[] = [];
    private requestQueue: Promise<void>[] = [];

    constructor(
        parsedMail: ParsedMail,
        smtpUser: SmtpUser
    ) {
        this.parsedMail = parsedMail;
        this.smtpUser = smtpUser;
    }

    private processAttachments() {
        if (
            this.parsedMail.attachments &&
            this.parsedMail.attachments.length > 0
        ) {
            this.attachments = this.parsedMail.attachments.map((item) => ({
                content: item.content,
                fileName: item.filename,
                contentType: item.contentType,
            }));
        }else {
            console.log('No Attachments Found')
        }
      
    }

    private createRequest(file: {
        content: Buffer;
        fileName?: string;
        contentType: string;
    }) {
        const formData = new FormData();
        formData.append('file', file.content, file.fileName);
        return axios({
            method: 'post',
            url: `https://${region}-${this.smtpUser.projectId}.cloudfunctions.net/sendToThreatDetect`,
            data: formData,
            headers: {
                'Content-Type': 'multipart/form-data',
                'promiseq-subscription-key': this.smtpUser.defaultKey,
                'check-video-integrity': false,
            }
        }).then(() => {
            console.log("SUCCESS");
        }).catch((err) => {
            console.log("ERROR", JSON.stringify(err))
        })
    }

    public createRequestQueue() {
        this.processAttachments()
        this.requestQueue = this.attachments.map((item) => this.createRequest(item));
    }

    async processRequestQueue() {
        await Promise.all(this.requestQueue);
    }
}
