import { AddressObject, EmailAddress, ParsedMail } from 'mailparser';
import { promises as dnsPromises } from 'dns';
import { createTransport, Transporter } from 'nodemailer';
import SMTPTransport from 'nodemailer/lib/smtp-transport';
import * as os from 'os';
import { getEmailAddressToText } from '../utils/helpers';
import { Attachment } from 'nodemailer/lib/mailer';

export default class DeliveryService {
    private parsedMail: ParsedMail;
    private transporterList: {emailAddress: EmailAddress, transporter: Transporter<SMTPTransport.SentMessageInfo>}[] = [];
    private mailQueue: Promise<any>[] = [];
    private toList: EmailAddress[] = [];
    private bccList: EmailAddress[] = [];
    private ccList: EmailAddress[] = [];
    private attachments: Attachment[] = []

    constructor(parsedMail: ParsedMail) {
        this.parsedMail = parsedMail;
    }

    async getMxAddress(emailAddress: EmailAddress): Promise<string> {
        try {
            const address = emailAddress.address;
            const addressHost = address!.split('@').pop();
            const mxAddress = await dnsPromises.resolveMx(addressHost!);
            if (mxAddress.length > 0) {
                return mxAddress[0].exchange;
            } else {
                throw new Error('No MX Address Found');
            }
        } catch (err) {
            console.error(err);
            throw new Error('DNS Resolve Failed');
        }
    }

    private getTransporter(
        host: string,
        port = 25
    ): Transporter<SMTPTransport.SentMessageInfo> {
        return createTransport({
            port: port,
            host: host,
            name: os.hostname(),
        });
    }

    private formatMessage() {
        const subject = this.parsedMail.subject;
        const attachments = this.parsedMail.attachments;
        const from = this.parsedMail.from?.text
        return {
            from,
            subject,
        };
    }

    private processEmailList(
        addressObject?: AddressObject | AddressObject[]
    ): EmailAddress[] {
        const currentList = [];
        if (addressObject) {
            if (Array.isArray(addressObject)) {
                addressObject.forEach((item) => {
                    currentList.push(...item.value);
                });
            } else {
                currentList.push(...addressObject.value);
            }
        }
        return currentList;
    }

    public processMessage() {
        this.toList = this.processEmailList(this.parsedMail.to);
        this.bccList = this.processEmailList(this.parsedMail.bcc);
        this.ccList = this.processEmailList(this.parsedMail.cc);
        this.attachments = this.parsedMail.attachments.map((item) => ({
            filename: item.filename,
            content: item.content
        }));
    }

    private async createSendRequest(emailAddress: EmailAddress) {
        try {
            const mxAddress = await this.getMxAddress(emailAddress);
            const transporter = this.getTransporter(mxAddress);
            this.transporterList.push({
                emailAddress,
                transporter
            });
        } catch (err) {
            console.log('Error createSendRequest', err);
        }
    }

    public async createEmailQueue() {
        const message = this.formatMessage();
        const allReceivers = [...this.toList, ...this.bccList, ...this.ccList];
         await Promise.all(
            allReceivers.map((item) => this.createSendRequest(item))
        );
        this.mailQueue = this.transporterList.map((item) => {
            const currentEmail = getEmailAddressToText(item.emailAddress)
            return item.transporter.sendMail({
                ...message,
                attachments: this.attachments,
                to: currentEmail
            }).then((res) => {
                console.log("Success", res);
            }).catch(err => {
                console.log('Error', err)
            })
        })
    }

    public async processEmailQueue() {
        await Promise.all(this.mailQueue)
    }
}
