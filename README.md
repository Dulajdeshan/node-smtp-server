# promiseQ SMTP Server (smtp_server)

SMTP Server built using nodemailer smtp-server library

## Features

- Authentication using PLAIN Auth 
- Using TLS/STARTTLS
- Allow Inbound SMTP Alarms
- Processing inbound SMTP and sending it to promiseQ platform

## Getting Started

To Start the Server:

- Clone the Repository
- Run `npm install` inside the repository
- Create `.env` file with following variables
```
CERT_PATH="PATH TO THE CERTIFICATE FILE"
KEY_PATH="PATH TO THE KEY FILE"
ELASTIC_ENVIRONMENT="Elastic Environment(development or production)"
``` 
- To start the server in debug mode run `npm run start:dev`
- To start the server in production mode run `npm run start`